# -*- encoding: utf-8 -*-

import socket
import email.utils
import os


def listFiles(path, uri):
    files = os.listdir(path)
    result = ""
    for item in files:
        result += "<a href='" + uri + "/" + item + "'/>" + item + "</a>"
        result += "<br/>"
    return result


def getContentType(uri):
    extension = uri[-3:]  # last 3 chars of filename
    contentType = ""
    if(extension == 'jpg' or extension == 'png'):
        contentType += "image/jpeg"
    elif(extension == 'txt'):
        contentType += "text/plain"
    return contentType


def http_serve(server_socket):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
            # Setting defaults
                header = "HTTP/1.1 200 OK"
                contentType = "text/html"
                date = str(email.utils.formatdate())
                body = ""

                req = request.split('\n')
                head = req[0].split(' ')
                uri = head[1].lstrip('/').rstrip('/') # obcinamy zbedne ukosniki
                print uri

                path = "/home/p15/dom/web/" + uri
                print path

                if(os.path.isdir(path)):
                    body += listFiles(path, uri)
                elif(os.path.isfile(path)):
                    contentType = getContentType(uri)
                    body += open(path).read()
                else:
                    header = "HTTP/1.1 404 Not Found"
                    body += "ERROR 404 FILE NOT FOUND"

                connection.send(header + "\r\n")
                connection.send("Content-Type: " + contentType + "; charset=utf-8" + "\r\n")
                connection.send("Content-Length: " + str(len(body)) + "\r\n")
                connection.send("Date: " + date + "\r\n\r\n")
                connection.send(body)

        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
# server_address = ('localhost', 8080)  # TODO: zmienić port!
server_address = ('194.29.175.240', 31015)
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

try:
    http_serve(server_socket)

finally:
    server_socket.close()

