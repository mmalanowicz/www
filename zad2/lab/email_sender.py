# -*- encoding: utf-8 -*-

import email.utils
from email.mime.text import MIMEText
import smtplib

def create_and_send_email():

    # Odczyt danych od użytkownika
    fromMail = raw_input("Podaj nadawce: ")
    toMail = raw_input("Podaj odbiorce: ")
    mailSubject = raw_input("Podaj temat: ")
    messageContent = raw_input("Podaj tresc wiadomosci: ")

    # Stworzenie wiadomości
    msg = MIMEText(messageContent, _charset='utf-8')
    msg['From'] = email.utils.formataddr(("Nadawca", fromMail))
    msg['To'] = email.utils.formataddr(("Odbiorca", toMail))
    msg['Subject'] = mailSubject

    try:
        # Połączenie z serwerem pocztowym
        server = smtplib.SMTP('194.29.175.240', 25)

        # Ustawienie parametrów
        server.set_debuglevel(True)

        # Autentykacja
        server.starttls()
        server.ehlo()
        server.login('p15', 'p15')

        # Wysłanie wiadomości
        server.sendmail(fromMail, [toMail], msg.as_string())
        pass

    finally:
        # Zamknięcie połączenia
        server.close()
        pass

decision = 't'

while decision in ['t', 'T', 'y', 'Y']:
    create_and_send_email()
    decision = raw_input('Wysłać jeszcze jeden list? ')
