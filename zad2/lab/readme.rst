
Laboratorium #2
===============

1. Zbudować interaktywny program konsolowy do wysyłania poczty elektronicznej w pliku ``email_sender.py``.

2. Zbudować prosty serwer HTTP w pliku ``http_server.py`` w oparciu o istniejący kod.
   * Zwracane poprawne nagłówki HTTP wraz z kodem ``200 OK``
   * Dodatkowy nagłówek ``GMT Date`` z datą w formacie RFC-1123 (zobacz ``email.utils.formatdate``)
   * Parsowanie URI i wypisywanie na konsolę
   * Walidacja żądania, czy rzeczywiście jest to protokół HTTP i metoda GET — jeżeli nie to zwraca odpowiedni błąd
