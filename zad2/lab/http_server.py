# -*- encoding: utf-8 -*-

import socket
import email.utils

def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie żądania
            request = connection.recv(1024)
            if request:
                req = str(request)
                header = "HTTP/1.1 200 OK"
                date = str(email.utils.formatdate())
                head = req.split('\n').pop(0)
                uri = head.split(' ').pop(1)

                print "URI: " + str(uri)


                if((head.find('GET') == -1) or (head.find('HTTP/1.1') == -1)):
                    print "To nie jest protokol http"
                else:
                    print "To jest protokol http"

                content = header + "\r\n" + "GMT Date: " + date + "\r\n" + "Content-Type: text/html" + "\r\n" + "Content-Length: " + str(len(html)) + "\r\n\r\n" + html

                # Wysłanie zawartości strony
                connection.sendall(content)

        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 8080)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

