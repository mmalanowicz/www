from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='entries'),
    url(r'^/(?P<user>\d{50})$', views.index, name='entries'),
    url(r'^add$', views.form, name='add'),
    url(r'^entry/submit$', views.submit_entry, name='submit_entry'),
    url(r'^login$', views.loginView, name='login'),
    url(r'^registry$', views.registry, name='registry'),
    url(r'^doRegistry$', views.do_registry, name='doRegistry'),
    url(r'^doLogin$', views.do_login, name='doLogin'),
    url(r'^logout$', views.logoutView, name='logout'),
    url(r'^denied$', views.denied, name='denied')
)
