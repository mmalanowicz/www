#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import requests
from celery import Celery
import time, random

celery = Celery('randomp13')


class Config:
    BROKER_URL = 'amqp://194.29.175.241'
    CELERY_RESULT_BACKEND = 'cache'
    CELERY_CACHE_BACKEND = 'memcached://194.29.175.241:11211'

celery.config_from_object(Config)


@celery.task
def sort_numbers(n):
    time.sleep(0.003)
    #url = "http://www.random.org/integers"
    #data = {'num': 100, 'min': 1, 'max': n, 'col': 1, 'base': 10, 'format': 'plain', 'rnd': 'new'}
    #response = requests.get(url, params=data)
    #print response.url
    numbers = []
    #for i in response.content:
    #    try:
    #        numbers.append(str(int(i)))
    #    except ValueError:
    #        pass
    #numbers = ' '.join(sorted(numbers))
    return "1 2 3 4 5"

if __name__ == '__main__':
    celery.start()