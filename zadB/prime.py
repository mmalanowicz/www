# -*- coding: utf-8 -*-
import memcache, random, time, timeit
from math import floor, sqrt
mc = memcache.Client(['194.29.175.241:11211', '194.29.175.242:11211'])

found = 0
total = 0


def rozklad(x):
    startNumber = x
    if x <= 0:
        return 0
    i = 2
    e = floor(sqrt(x))
    r = [] #używana jest tablica (lista), nie bepośrednie wypisywanie
    while i <= e:
        if x % i == 0:
            r.append(i)
            x /= i
            e = floor(sqrt(x))
        else:
            i += 1
    if x > 1:
        r.append(x)

    value = ""


    if startNumber in r:
        return str(x)
    else:
        ' '.join(r)
    return value


def main(n):
    global total, found
    total += 1
    value = mc.get('prime:%d' % n)
    if value is None:
        time.sleep(0.003)
        primeNumbers = rozklad(n)
        mc.set('prime:%d' % n, primeNumbers)
    else:
        found += 1
    return value


def make_request():
    main(random.randint(0, 5000))

print 'Ten successive runs:',
for i in range(1, 11):
    print '%.2fs, ratio=%.2f' % (timeit.timeit(make_request, number=2000), float(found) / total)
    print
