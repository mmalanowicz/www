# -*- coding: utf-8 -*-
import memcache, random, time, timeit, requests
from math import floor, sqrt
#mc = memcache.Client(['194.29.175.241:11211', '194.29.175.242:11211'])

found = 0
total = 0


def rozklad(x):
    url = "http://www.random.org/integers"
    randomNum = random.randint(10, 500)
    data = {'num': 100, 'min': 1, 'max': randomNum, 'col': 1, 'base': 10, 'format': 'plain', 'rnd': 'new'}
    response = requests.get(url, params=data)
    #print response.url
    numbers = []
    for i in response.content:
        try:
            numbers.append(int(i))
        except ValueError:
            pass
    numbers = sorted(numbers)
    print numbers


def main(n):
    global total, found
    total += 1
    rozklad(n)
    #value = mc.get('prime:%d' % n)
    # if value is None:
    #     time.sleep(0.003)
    #     primeNumbers = rozklad(n)
    #     #mc.set('prime:%d' % n, primeNumbers)
    # else:
    #     found += 1
    # return value


def make_request():
    main(random.randint(0, 5000))

print 'Ten successive runs:',
for i in range(1, 11):
    print '%.2fs, ratio=%.2f' % (timeit.timeit(make_request, number=2000), float(found) / total)
    print
