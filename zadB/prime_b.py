import random, timeit, uuid
from celery import group
import p13_prime_worker


def compute_square(n):
    return p13_prime_worker.sort_numbers.delay(n)


def make_request():
    return compute_square(random.randint(0, 5000))


def requests_suite():
    results = [make_request() for i in xrange(2)]
    group(results)().get()


print 'Ten successive runs:',
for i in range(1, 2):
    print '%.2fs' % timeit.timeit(requests_suite, number=1)
    print