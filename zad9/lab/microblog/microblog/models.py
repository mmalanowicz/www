# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent


def Blog(PersistentMappig):
    """ Klasa dla kontenera """
    # Należy ją odpowiednio zainicjalizować
    pass


def Post(Persistent):
    """ Klasa dla pojedynczego wpisu """
    # Należy dorobić odpowiedni konstruktor


def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # Utworzenie głównegp kontenera
        app_root = Blog()
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
