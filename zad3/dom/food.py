# -*- encoding: utf-8 -*-

import requests
import sys
import webbrowser

reload(sys)
sys.setdefaultencoding('utf-8') #  ustawiamy kodowanie

key = 'AIzaSyAo52p3KrqZWBVYnIeSsXMUBYzvNo2p4kw'  # TODO ZMIEN KLUCZ!

url = {
    'google': 'http://maps.googleapis.com/maps/api/geocode/json',
    'places': 'https://maps.googleapis.com/maps/api/place/nearbysearch/json',
    'staticMap': 'http://maps.googleapis.com/maps/api/staticmap'
}

print "Program pokazuje mape z max 20 \'restauracjami\' ktore znajduja sie w odleglosci podanej przez uzytkownika."
address = raw_input('Podaj adres(bez polskich znakow): ')
distance = raw_input('Podaj odleglosc w metrach(max 5 000m): ') # maxem google api jest 50km, ale wtedy mapa jest nieczytelna

try:
    distance = int(float(distance))
    if distance > 5000:
        raise ValueError
except ValueError:
    print "Podaj poprawna liczbe"
    sys.exit(-1)

# Znalezienie współrzędnych adresu
data = {'address': address, 'sensor': 'false'}
print data
response = requests.get(url.get('google'), params=data)
results = response.json().get('results')
if not results:  # results are empty
    print "Nie znaleziono Twojego adresu..."
    sys.exit(-1)
location = results[0].get('geometry').get('location')
coords = str(location.get('lat')) + "," + str(location.get('lng'))

# Znalezienie najbliższych punktow z jedzeniem
data = {'location': coords, 'radius': distance, 'types': 'food', 'sensor': 'false', 'key': key}
response = requests.get(url.get('places'), params=data)
respJson = response.json()
if respJson.get('status') != 'OK':
    print "Niestety nic nie znaleziono w poblizu..."
    sys.exit(-1)
results = respJson.get('results')

#Wyswietlenie nazw punktow wraz z adresami
for result in results:
    location = result.get('geometry').get('location')
    print result.get('name') + ' - ' + result.get('vicinity')

#Wygenerowanie stringa z koordynatami punktow
markers = ""
for result in results:
    location = result.get('geometry').get('location')
    markers += str(location.get('lat')) + "," + str(location.get('lng')) + "|"
markers = markers[:-1]  # usuniecie ostatniej 'palki'

#Stworzenie url i otworzenie przegladarki
url = url.get('staticMap') + '?' + 'center=' + coords + '&size=600x600&markers=' + markers\
    + '&sensor=false&key=' + key
webbrowser.open(url)