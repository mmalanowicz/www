# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup

url = {
    'login': 'https://e.sggw.waw.pl/login/index.php',
    'grades': 'http://e.sggw.waw.pl/grade/report/user/index.php?id=649'
}

username = raw_input('Użytkownik: ')
password = getpass.getpass('Hasło: ')

payload = {
    'username': username,
    'password': password
}

# Logowanie
session = requests.session()
session.post(url.get('login'), data=payload, verify=False)

# Pobranie strony z ocenami
response = session.get(url.get('grades'), verify=False)

# Odczyt strony
html = response.text

# Parsowanie strony
parsed = BeautifulSoup(html)

# Scraping
grades = parsed.find('table', class_='user-grade')
tbody = grades.find('tbody')
trs = tbody.find_all('tr')
items = {}
tasks = []
categoryName = ""

for tr in trs:
    catImg = tr.find('img', title='Kategoria')
    zad = tr.find('a')
    podsumowanie = tr.find('td', class_='baggt')

    if catImg is not None:
        categoryName = catImg.find_parent('td').text
    if zad is not None:
        task = {}

        points = tr.find_all('td')[1].text
        if points != '-':
            points = points.replace(',', '.')
            points = float(points)
        else:
            points = 0.0

        task.update({'points': points})
        task.update({'taskName': zad.text})

        href = zad['href']
        task.update({'link': href})

        tasks.append(task)
    if podsumowanie is not None:
        items.update({categoryName: tasks})
        categoryName = ""
        tasks = []

# Sortowanie
sortedItems = {}
for key, value in items.iteritems():
    value = sorted(value, key=lambda k: k['points'], reverse=True)
    sortedItems.update({key: value})

# Wyświetlenie posortowanych ocen w kategoriach
for key, value in sortedItems.iteritems():
    print key
    for val in value:
        print val.get('taskName') + ": " + str(val.get('points')) + "pts."
