# -*- encoding: utf-8 -*-

import requests
from pprint import pprint

key = 'WEPWXg9HEUGfHDGvyaVg'

url = {
    'google': 'http://maps.googleapis.com/maps/api/geocode/json',
    'nearest': 'http://opencaching.pl/okapi/services/caches/search/nearest',
    'geocaches': 'http://opencaching.pl/okapi/services/caches/geocaches'
}

address = raw_input('Podaj adres: ')

# Znalezienie współrzędnych adresu
data = {'address': address, 'sensor': 'false'}
response = requests.get(url.get('google'), params=data)
results = response.json().get('results')
location = results[0].get('geometry').get('location')
mycords = str(location.get('lat')) + "|" + str(location.get('lng'))

# Znalezienie najbliższych geoskrytek
data = {'center': '52.16|21.04', 'consumer_key': key}
response = requests.get(url.get('nearest'), params=data)
results = response.json()

# Wydobycie informacji o lokalizacji skrytek
caches = '|'.join(results['results'])
data = {'cache_codes': caches, 'consumer_key': key}
response = requests.get(url.get('geocaches'), params=data)
results = response.json()

# Przydzielenie adresu skrytkom na podstawie ich współrzędnych
treasures = {}
for key, value in results.iteritems():
    name = value.get('name')

    location = value.get('location')
    data = {'latlng': location.replace('|', ','), 'sensor': 'false'}
    response = requests.get(url.get('google'), params=data)
    results = response.json().get('results')

    address = "ERROR"
    if len(results) > 0:
        address = results[0].get('formatted_address')

    treasures = {name: address}
# Wyświetlenie wyniku
    for key, value in treasures.iteritems():
        print key + " - " + value