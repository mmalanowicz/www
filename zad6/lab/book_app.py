# -*- coding: utf-8 -*-

from flask import *
import bookdb
import sqlite3
from contextlib import *

DATABASE = 'book.db'
SECRET_KEY = '1234567890!@#$%^&*()'

app = Flask(__name__)
app.config.from_object(__name__)

db = bookdb.BookDB()


def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()


def titles():
    cur = g.db.execute('select id, title from book order by id desc')
    # Stworzenie słownika dla każdego odczytanego wiersza oraz upakowanie ich w listę
    books = [dict(id = row[0], title=row[1]) for row in cur.fetchall()]
    return books


def insert():
    g.db.execute('insert into book (title,isbn,publisher,author) values (?, ?, ?, ?)',
                 ['test', 'test', 'test', 'test'])
    g.db.commit()


def title_info(book_id):
    cur = g.db.execute('select id, title, author, isbn, publisher from book where id = ' + book_id + ' order by id desc')
    # Stworzenie słownika dla każdego odczytanego wiersza oraz upakowanie ich w listę
    book = [dict(id=row[0], title=row[1], author = row[2], isbn=row[3], publisher = row[4]) for row in cur.fetchall()]
    return book[0]


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    g.db.close()

@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    return render_template('book_list.html', books=titles())



@app.route('/book/<book_id>/')
def book(book_id):
    return render_template('book_detail.html', book=title_info(book_id))

if __name__ == '__main__':
    app.run(debug=True)