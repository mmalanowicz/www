drop table if exists book;
create table book (
    id integer primary key autoincrement,
    title string not null,
    isbn string not null,
    publisher string not null,
    author string not null
);