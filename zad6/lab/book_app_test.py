# -*- coding: utf-8 -*-

import os
import book_app
import unittest
import tempfile


class BookAppTestCase(unittest.TestCase):

    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        self.app = book_app.app
        self.app.config['TESTING'] = True
        self.client = self.app.test_client()
        # stworzenie tymczasowego pliku na bazę testową
        # i pobranie uchwytu do pliku oraz jego nazwy
        self.db_fd, self.app.config['DATABASE'] = tempfile.mkstemp()
        # inicjalizacja bazy - dodać kod
        book_app.init_db()

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        # zamknięcie tymczasowego pliku z bazą testową
        os.close(self.db_fd)
        # skasowanie tymczasowego pliku z bazą testową
        os.unlink(self.app.config['DATABASE'])

    def test_database_setup(self):
        """
        Testuje czy baza została poprawnie zainicjalizowana.
        """
        con = book_app.connect_db()
        # Pobranie informacji o tebeli entries
        cur = con.execute('PRAGMA table_info(book);')
        rows = cur.fetchall()
        # Tabela entries powinna mieć 3 kolumny
        self.assertEquals(len(rows), 5)


    def test_get_all_books_empty(self):
        """
        Testuje brak książek w nowej bazie.
        """
        # Symulacja żądania z URL = /
        response = self.client.get('/')
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            assert u'Brak książek' in unicode(response.data, 'utf-8')
            pass

    def test_get_all_books(self):
        """
        Testuje odczyt z bazy wszystkich książek.
        """
        # Symulacja żądania z URL = /

        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            book_app.insert()
            response = self.client.get('/')
            assert u'test' in unicode(response.data, 'utf-8')

    def test_empty_listing(self):
        """
        Testuje odpowiedź strony głównej przy braku książek.
        """
        response = self.client.get('/')
        print response.data
        assert u'Brak książek' in unicode(response.data, 'utf-8')

    def test_listing(self):
        """
        Testuje odpowiedź strony głównej ze znanymi książkami.
        """
        # Symulacja żądania z URL = /


        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            book_app.insert()
        response = self.client.get('/')
        print response.data
        assert u'test' in unicode(response.data, 'utf-8')

    def test_details(self):
        """
        Testuje odpowiedź dla szczegółów konkretnej książki.
        """
        # dodać książki
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            book_app.insert()

        response = self.client.get('book/1', follow_redirects=True)
        print response.data
        assert u'Title: test' in unicode(response.data, 'utf-8')

    def test_details_error(self):
        """
        Testuje odpowiedź dla szczegółów błędnej książki.
        """
        # dodać książki
        response = self.client.get('/id33')
        print vars(response)
        assert u'404 NOT FOUND' in unicode(response.status, 'utf-8')
        # dodać kod
        pass



if __name__ == '__main__':
    unittest.main()