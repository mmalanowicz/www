# -*- coding: utf-8 -*-

import os
import app
import requests
import unittest
import tempfile
import bottle


class BlogAppTestCase(unittest.TestCase):

    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        self.app = app
        # stworzenie tymczasowego pliku na bazę testową
        # i pobranie uchwytu do pliku oraz jego nazwy
        self.db_fd, self.app.DATABASE = tempfile.mkstemp()
        # inicjalizacja bazy - dodać kod
        app.init_db()

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        # zamknięcie tymczasowego pliku z bazą testową
        os.close(self.db_fd)
        # skasowanie tymczasowego pliku z bazą testową
        os.unlink(self.app.DATABASE)

    def test_database_setup(self):

        con = app.dbs()
        # Pobranie informacji o tebeli entries
        cur = con.execute('PRAGMA table_info(entries);')
        rows = cur.fetchall()
        # Tabela entries powinna mieć 3 kolumny
        self.assertEquals(len(rows), 4)


    def test_get_all_entries(self):
        self.app.do_registry('test','test')
        self.app.response.set_cookie("user", str(1))
        self.app.write_entry('Test','test')
        print self.app.index()



    #assert u'Tytul' in unicode(response.data, 'utf-8')

    def test_empty_listing(self):
        response = app.index()
        assert u'No entries' in response
      #  assert u'No entries' in unicode(response.data, 'utf-8')



    def test_messages(self):
        pass

    def test_auth(self):
        assert u'Nie masz uprawnień ' in unicode(app.add(), 'utf-8')

    def test_auth(self):
        self.app.do_registry('test', 'test')
        self.assertTrue(self.app.check_login('test','test'))
        self.assertFalse(self.app.check_login('fake','fake'))

if __name__ == '__main__':
    unittest.main()