# -*- coding: utf-8 -*-
from datetime import datetime
from bottle import route, run, static_file, template, request, redirect, response, error, url
import os
import sqlite3
from contextlib import closing

DATABASE = 'blog.db'

@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./static')


@route('/login')  # formularz do logowania
def loginView():
    if isLogged():
        return template('index', entries=get_all_entries())
    else:
        return template('login', msg='')


@route('/doLogin', method='POST')
def login():
    username = request.forms.get('login')
    password = request.forms.get('password')
    try:
        check_login(username, password)
    except ValueError:
        return template('login', msg='Wrong login or password. Try again.')
    except NameError:
        return template('login', msg="User doesn't exist.")
    response.set_cookie("user", str(get_user(username)['id']))
    redirect('/')


@route('/registry')  # rejestracja
def registryView():
    return template('registry')


@route('/doRegistry', method='POST')
def registry():
    login = request.forms.get('login')
    password = request.forms.get('password')
    do_registry(login, password)
    return template("login", msg='Registred successfully.')


@route('/')  # strona z wpisami
def index():
    try:
        user = get_current_username()
        return template('index', entries=get_all_entries(), user=user)
    except NameError:
        return template('index', entries=None, user = user)



@route('/add')  # strona z dodawaniem postow
def add():
    if isLogged():
        user = get_current_username()
        return template('add', user=user)
    else:
        redirect('/denied')


@route('/entry/submit', method="POST")
def submitEntry():
    if isLogged():
        title = request.forms.get('title')
        content = request.forms.get('content')
        write_entry(title, content)
        redirect('/')
    else:
        redirect('/denied')


@route('/denied')
def accessDenied():
    return template('denied')


@route('/logout')
def logout():
    response.delete_cookie("user")
    return template('login', msg='Logged out successfully.')

@error(404)
def error404(error):
    return 'Page not found.'


########################################################################
################
#   functions   #
################
def check_login(username, password):
    usr = get_user(username)
    if username != usr['username']:
        raise ValueError
    elif password != usr['password']:
        raise ValueError


def isLogged():
    if request.get_cookie("user"):
        return True
    else:
        return False


def get_current_username():
    if not request.get_cookie("user"):
        return ''
    try:
        user = get_user_byId(request.get_cookie("user"))['username']
    except NameError:
        return  ''
    return user



################
#   database   #
################
def dbs():
    return sqlite3.connect(DATABASE)


def init_db():
    try:
        with closing(dbs()) as db:
            with open('schema.sql', 'rt') as f:
                schema = f.read()
                db.executescript(schema)
            db.commit()
    except:
        pass


def do_registry(login, password):
    with dbs() as db:
        db.execute('insert into users (username, password) values (?, ?)',
                   [login, password])
    db.commit()


def get_user(username):
    cur = dbs().execute('select id,username, password from users where username = "' + username + '"')
    row = cur.fetchone()
    if row is None:
        raise NameError
    user = dict(username=row[1], password=row[2], id=row[0])
    return user


def get_user_byId(user_id):
    cur = dbs().execute('select username from users where id = "' + str(user_id) + '"')
    row = cur.fetchone()
    if row is None:
        raise NameError
    user = dict(username=row[0])
    return user


def get_all_entries():
    cur = dbs().execute('select title, text, user_id, id from entries order by id desc')
    entries = [dict(title=row[0], text=row[1], author=get_user_byId(row[2])['username'], id=row[3])
               for row in cur.fetchall()]
    return entries


def get_entry(entry_id):
    cur = dbs().execute('select * from entries where id = ' + entry_id)
    row = cur.fetchone()
    entry = dict(id=row[0], title=row[1], text=row[2], author=get_user_byId(row[3]))
    return entry


def write_entry(title, text):
    user_id = request.get_cookie("user")
    with dbs() as db:
        db.text_factory = str
        db.execute('insert into entries (title, text, user_id) values (?, ?, ?)',
                   [title, text, user_id])
    db.commit()

if __name__ == '__main__':
    init_db()
    run(host='194.29.175.240', port=31014)
