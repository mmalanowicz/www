create table entries (
    id integer primary key autoincrement,
    title string,
    text string,
    user_id integer
);
create table users (
  id integer primary key autoincrement ,
  username string,
  password string
);