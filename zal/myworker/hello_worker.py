import requests, random
import argparse
import json


# parser = argparse.ArgumentParser(
#     description="Calculates the Fibonacci sequence up to a maximum number")
# parser.add_argument("-payload", type=str, required=False,
#                     help="The location of a file containing a JSON payload.")
# args = parser.parse_args()
#
# pd = False
# if args.payload is not None:
#     payload = json.loads(open(args.payload).read())
#     if 'query' in payload:
#         query = payload['query']


def getRandomNumbers():
    #http://www.random.org/clients/http/
    url = "http://www.random.org/integers"
    #randomNum = random.randint(10, 500)
    data = {'num': 100, 'min': 1, 'max': 500, 'col': 1, 'base': 10, 'format': 'plain', 'rnd': 'new'}
    response = requests.get(url, params=data)
    #print response.url
    responseContent = response.content.split('\n')
    numbers = []
    for i in responseContent:
        try:
            numbers.append(int(i))
        except ValueError:
            pass
    numbers = sorted(numbers)
    print numbers
    return numbers
    #print responseContent

getRandomNumbers()