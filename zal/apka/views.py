# -*- encoding: utf-8 -*-
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
import requests
import couchdb
import json


def index(request):
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch['calc']

    url = 'http://194.29.175.241:5984/calc/_design/utils/_view/list_active'
    response = requests.get(url=url)
    data = json.loads(response.content)
    found = False
    operacje = []
    for row in data['rows']:
        if row['value']['host'] == "http://pwi.eu01.aws.af.cm/":
            doc_id = row['id']
            found = True
        if row['value']['active'] is True:       # tylko aktywne
            operator = row['value']['operator']
            if not operator in operacje:
                operacje.append(operator)

    doc = {'host': 'http://pwi.eu01.aws.af.cm/', 'active': True, 'operator': '-', 'calculation': 'http://pwi.eu01.aws.af.cm/calculateMinus'}
    if found is True:
        doc = db[doc_id]
        doc['active'] = True
        db[doc_id] = doc
    else:
        db.save(doc)

    return render(request, 'index.html', {'operacje': operacje})


def calculateMinus(request):
    id = request.POST.get('id', '')
    number1 = request.POST.get('number1', '')
    number2 = request.POST.get('number2', '')
    try:
        result = float(number1) - float(number2)
        return result
    except ValueError:
        return None


def calculate(request):
    text = request.POST['calc']
    text = text.split(' ')
    stos = []
    try:
        for i in text:
            try:
                stos.append(float(i))
                print stos
            except ValueError:
                if i == '+':
                    stos.append(stos.pop(len(stos) - 2) + stos.pop(len(stos) - 1))
                elif i == '-':
                    stos.append(stos.pop(len(stos) - 2) - stos.pop(len(stos) - 1))
                elif i == '*':
                    stos.append(stos.pop(len(stos) - 2) * stos.pop(len(stos) - 1))
                elif i == '/':
                    stos.append(stos.pop(len(stos) - 2) / stos.pop(len(stos) - 1))
        print stos
        if len(stos) > 1:
            messages.error(request, 'Zly ciag znakow. Oddziel znaki spacjami.')
        else:
            messages.success(request, 'Wynik to ' + str(stos.pop(0)))
    except IndexError:
        messages.error(request, 'Zly ciag znakow. Oddziel znaki spacjami.')
    return HttpResponseRedirect(redirect_to=reverse('index'))


def loginView(request):
    if not request.user.is_authenticated():
        return render(request, 'login.html')
    else:
        return HttpResponseRedirect(redirect_to=reverse('index'))


def do_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return HttpResponseRedirect(redirect_to=reverse('index'))
    else:
        messages.error(request, 'Wrong username or password.')
        return HttpResponseRedirect(redirect_to=reverse('login'))


def registry(request):
    return render(request, 'registry.html')


def do_registry(request):
    username = request.POST['username']
    password = request.POST['password']
    try:
        User.objects.create_user(username, None, password).save()
    except IntegrityError:
        messages.error(request, 'User already exists.')
        return redirect('/blog/registry')
    messages.success(request, 'Registered successfully.')
    return HttpResponseRedirect(redirect_to=reverse('login'))


def logoutView(request):
    logout(request)
    return HttpResponseRedirect(redirect_to=reverse('login'))


def denied(request):
    return render(request, 'microblog/denied.html')