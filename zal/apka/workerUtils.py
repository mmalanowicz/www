from iron_worker import *
import requests


def preparePackage(worker):
    package = CodePackage()
    package.name = "MyWorker"
    package.merge("myworker", ignoreRootDir=True)
    package.merge_dependency("requests")
    package.executable = "hello_worker.py"

    worker.upload(package)


def getLog(projectId, taskId):
    url = "https://worker-aws-us-east-1.iron.io/2/projects/" + projectId + "/tasks/" + taskId + "/log" + "?oauth=sVFKmjblZI1neD6NdQkpDWLHCJM"
    response = requests.get(url)
    print response.content