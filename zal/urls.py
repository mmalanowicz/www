from django.conf.urls import patterns, url
from apka import views

urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^calculate$', views.calculate, name='calculate'),
    url(r'^calculateMinus$', views.calculateMinus, name='calculateMinus'),
    url(r'^login$', views.loginView, name='login'),
    url(r'^registry$', views.registry, name='registry'),
    url(r'^doRegistry$', views.do_registry, name='doRegistry'),
    url(r'^doLogin$', views.do_login, name='doLogin'),
    url(r'^logout$', views.logoutView, name='logout'),
    url(r'^denied$', views.denied, name='denied')
)
