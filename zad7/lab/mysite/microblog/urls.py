from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='entries'),
    url(r'^add$', views.form, name="add"),
    url(r'^entry/submit$', views.submit_entry, name='submit_entry')
)
