# Create your views here.
from django.shortcuts import render
from models import Entry


def index(request):
    entries = Entry.objects.all().order_by('title')
    context = {'entries': entries}
    return render(request, 'microblog/index.html', context)


def form(request):
    return render(request, 'microblog/add.html')


def submit_entry(request):
    title = request.POST['title']
    text = request.POST['content']
    author = request.POST['author']
    Entry(title=title, text=text, author=author).save()
    return render(request, 'microblog/index.html')
