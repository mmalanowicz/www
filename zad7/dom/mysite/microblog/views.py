from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.contrib import messages
from models import Entry


def index(request):
    user = request.GET.get('user', '')
    if user is '':
        entries = Entry.objects.all().order_by('-pk')
    else:
        entries = Entry.objects.filter(author=user).order_by('-pk')
    users = User.objects.all()
    context = {'entries': entries, 'users': users}
    return render(request, 'microblog/index.html', context)


def form(request):
    if not request.user.is_authenticated():
        return redirect('/blog/denied')
    else:
        return render(request, 'microblog/add.html')


def submit_entry(request):
    if request.user.is_authenticated():
        title = request.POST['title']
        text = request.POST['content']
        author = request.user
        Entry(title=title, text=text, author=author).save()
        return redirect('/blog')
    else:
        return redirect('/blog/denied')


def loginView(request):
    if not request.user.is_authenticated():
        return render(request, 'microblog/login.html')
    else:
        return redirect('/blog')


def do_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/blog')
    else:
        messages.error(request, 'Wrong username or password.')
        return redirect('/blog/login')


def registry(request):
    return render(request, 'microblog/registry.html')


def do_registry(request):
    username = request.POST['username']
    password = request.POST['password']
    try:
        User.objects.create_user(username, None, password).save()
    except IntegrityError:
        messages.error(request, 'User already exists.')
        return redirect('/blog/registry')
    messages.success(request, 'Registered successfully.')
    return redirect('/blog/login')


def logoutView(request):
    logout(request)
    return redirect('/blog')


def denied(request):
    return render(request, 'microblog/denied.html')

