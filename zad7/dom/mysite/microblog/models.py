from django.db import models


class Entry(models.Model):
    title = models.CharField(max_length=100)
    text = models.CharField(max_length=500)
    author = models.CharField(max_length=50)

    def __unicode__(self):
        return self.text
