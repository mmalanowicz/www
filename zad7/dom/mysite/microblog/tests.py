import unittest
from django.test import TestCase
from django.contrib.auth import authenticate, login
from models import Entry


class MyTestCase(TestCase):
    def setUp(self):
        self.title = "title"
        self.text = "text"
        self.author = "author"
        self.message = Entry.objects.create(title=self.title, text=self.text, author=self.author)

    def test_database(self):
        """
        Sprawdzamy czy wpis sie dodal do bazy
        """                                                                                                     ""
        self.assertEquals(unicode(self.message.text), self.text)
        self.assertEquals(unicode(self.message.title), self.title)
        self.assertEquals(unicode(self.message.author), self.author)

    def test_blog_shows_entries(self):
        mainPage = self.client.get('/blog', follow=True)
        self.assertContains(mainPage, self.author)

    def test_blog_shows_user_entries(self):
        self.message = Entry.objects.create(title=self.title, text=self.text, author='admin')
        mainPage = self.client.get('/blog', follow=True)
        self.assertContains(mainPage, 'admin')

    def test_user_is_authenticated(self):
        url = '/blog/doRegistry'
        self.client.post(url, {'username': 'test', 'password': 'pass'})
        user = authenticate(username='test', password='pass')
        self.assertTrue(user)

    def test_blog_allows_to_add_entries(self):
        Entry.objects.create(title='alabala', text=self.text, author='admin')
        mainPage = self.client.get('/blog', follow=True)
        self.assertContains(mainPage, 'alabala')

    def test_blog_shows_error_messages(self):
        addPage = self.client.get('/blog/add', follow=True)
        self.assertContains(addPage, 'Access denied')


if __name__ == '__main__':
    unittest.main()
