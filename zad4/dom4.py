# -*- encoding: utf-8 -*-

import time
import os

from daemon import runner
from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler

########################
### GLOBAL VARIABLES ###
########################

PAGE = '<html><body>%s</body></html>'

LINK = '<a href="%s">%s</a>'

ROOT = '/home/pawel/zad4/web'

#################
### FUNCTIONS ###
#################

class RequestHandler(BaseRequestHandler):

	def __parse_init_line__(self, http):
		try:
			method, path, protocol = http.split('\r\n')[0].split(' ')
		except:
			raise Exception('Bad request!')
		
		if method.lower() != 'get' or protocol.lower() != 'http/1.1':
			raise Exception('Bad request!')
		
		return path

	def __update_dir_resp__(self, path):
		if path != '/': path += '/'
		
		resp = 'Content-Type: text/html\r\n\r\n'
		resp += '<html><body><ul>'
		for filename in os.listdir(ROOT + path):
			resp += '<li>' + (LINK % (path + filename, filename)) + '</li>'
		resp += '</ul></body></html>'
		
		return resp

	def __update_file_resp__(self, path):
		base, ext = os.path.splitext(path)
	
		if ext == '.html': resp = 'Content-Type: text/html\r\n\r\n'
		elif ext == '.txt': resp = 'Content-Type: text/plain\r\n\r\n'
		elif ext == '.jpg': resp = 'Content-Type: image/jpeg\r\n\r\n'
		elif ext == '.png': resp = 'Content-Type: image/png\r\n\r\n'
		else: raise Exception('File type not supported.')
		
		resp += open(path).read()
	
		return resp

	def handle(self):
		try:
			path = self.__parse_init_line__(self.request.recv(8192))
			if path != '/favicon.ico':
				resp = 'HTTP/1.1 200 OK\r\n'
				resp += 'Date: ' + str(time.ctime()) + '\r\n'
				resp += self.__update_dir_resp__(path) if os.path.isdir(ROOT + path) else self.__update_file_resp__(ROOT + path)
				try:
					self.request.sendall(resp)
				except:
					pass
		except:
			self.request.sendall('HTTP/1.1 404 Page not found' + '\r\n\r\n' + (PAGE % '404 Error<br />Page not found.'))
		finally:
			self.request.close()

class Server(ThreadingMixIn, TCPServer):
	allow_reuse_address = 1

class App:

	def __init__(self):
		self.stdin_path = '/dev/null'
		self.stdout_path = '/dev/tty'
		self.stderr_path = '/dev/tty'
		self.pidfile_path = '/tmp/daemon.pid'
		self.pidfile_timeout = 5

	def run(self):
		Server(('127.0.0.1', 31011), RequestHandler).serve_forever()

############
### MAIN ###
############

if __name__=='__main__':
	runner.DaemonRunner(App()).do_action()
