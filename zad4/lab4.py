# -*- encoding: utf-8 -*-

import socket
import time
import os
import logging

from daemon import runner

########################
### GLOBAL VARIABLES ###
########################

PAGE = '<html><body>%s</body></html>'

LINK = '<a href="%s">%s</a>'

ROOT = '/home/pawel/zad4/web'

##################
### EXCEPTIONS ###
##################

class ParsingError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class ExtentionError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

#################
### FUNCTIONS ###
#################

class Server:

	### PRIVATE ###

	# SETUP #

	def __setup__(self):
		self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.server.bind(('127.0.0.1', 31011))
		self.server.listen(1024)

	# INIT #

	def __init__(self, log):
		self.log = log
		
		self.stdin_path = '/dev/null'
		self.stdout_path = '/dev/tty'
		self.stderr_path = '/dev/tty'
		self.pidfile_path = '/tmp/deamon.pid'
		self.pidfile_timeout = 5

	# PARSE INIT LINE #

	def __parse_init_line__(self, http):
		try:
			method, path, protocol = http.split('\r\n')[0].split(' ')
		except:
			raise ParsingError('Bad request!')
		
		if method.lower() != 'get' or protocol.lower() != 'http/1.1':
			raise ParsingError('Bad request!')
		
		return path

	# UPDATE DIR RESP #

	def __update_dir_resp__(self, path):
		if path != '/': path += '/'
		
		resp = 'Content-Type: text/html\r\n\r\n'
		resp += '<html><body><ul>'
		for filename in os.listdir(ROOT + path):
			resp += '<li>' + (LINK % (path + filename, filename)) + '</li>'
		resp += '</ul></body></html>'
		
		return resp

	# UPDATE FILE RESP #

	def __update_file_resp__(self, path):
		base, ext = os.path.splitext(path)
	
		if ext == '.html':
			resp = 'Content-Type: text/html\r\n\r\n'
		elif ext == '.txt':
			resp = 'Content-Type: text/plain\r\n\r\n'
		elif ext == '.jpg':
			resp = 'Content-Type: image/jpeg\r\n\r\n'
		elif ext == '.png':
			resp = 'Content-Type: image/png\r\n\r\n'
		else:
			raise ExtentionError('File type not supported.')
		
		resp += open(path).read()
	
		return resp

	# HTTP SERVE #

	def __http_serve__(self):
		while True:
			conn, addr = self.server.accept()
			self.log.info('Client was connected. Addr=' + addr[0] + ".")
			try:
				path = self.__parse_init_line__(conn.recv(8192))
				self.log.info('Request was received.')
				if path != '/favicon.ico':
					resp = 'HTTP/1.1 200 OK\r\n'
					resp += 'Date: ' + str(time.ctime()) + '\r\n'
					resp += self.__update_dir_resp__(path) if os.path.isdir(ROOT + path) else self.__update_file_resp__(ROOT + path)
					try:
						conn.sendall(resp)
						self.log.info('Response was sent.')
					except:
						self.log.error('Response sending problem.')
			except ParsingError:
				self.log.warning('Bad request was sent by client.')
				conn.sendall('HTTP/1.1 400 bad request' + '\r\n\r\n' + (PAGE % '400 Error<br />Bad request.'))
			except ExtentionError:
				self.log.warning('Request for unsupported resource type was received.')
				conn.sendall('HTTP/1.1 404 Page not found' + '\r\n\r\n' + (PAGE % '404 Error<br />Page not found.'))
			except:
				self.log.error('Unknown exception was reached.')
				conn.sendall('HTTP/1.1 404 Page not found' + '\r\n\r\n' + (PAGE % '404 Error<br />Page not found.'))
			finally:
				self.log.info('Connection was closed. Addr=' + addr[0] + ".")
				conn.close()

	### PUBLIC ###

	# RUN #

	def run(self):
		try:
			self.__setup__()
			self.__http_serve__()
		finally:
			self.log.critical('Server was closed unexpectedly.')
			self.server.close()

############
### MAIN ###
############

if __name__=='__main__':
	log = logging.getLogger("Server")
	log.setLevel(logging.DEBUG)
	handler = logging.FileHandler("./logs.out")
	handler.setFormatter(logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
	log.addHandler(handler)
	
	server = Server(log)
	daemonRunner = runner.DaemonRunner(server)
	daemonRunner.daemon_context.files_preserve = [handler.stream]
	daemonRunner.do_action()
