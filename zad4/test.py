#!/usr/bin/env python
# -*- coding: utf-8 -*-

from funkload.FunkLoadTestCase import FunkLoadTestCase
import socket, os, unittest

class TestTest(FunkLoadTestCase):
    def test_dialog(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('194.29.175.240', 31002))
        sock.sendall('GET / HTTP/1.0\r\n')
        sock.sendall('GET /images/jpg_rip.jpg HTTP/1.0\r\n')
        sock.sendall('GET /images/test HTTP/1.0\r\n')
        sock.close()



if __name__ == '__main__':
    unittest.main()
