# -*- encoding: utf-8 -*-

import socket

gniazdo = socket.socket()

server_address= ('194.29.175.240', 31016)
gniazdo.bind(server_address)

gniazdo.listen(1)

suma = 0.0
numbersCount = 0

while True:
    connection, client_address = gniazdo.accept()

    try:
        while True:
            data = connection.recv(4096)
            print("Liczba: " + data)
            suma += float(data)
            numbersCount += 1
            if(numbersCount == 2):
                connection.sendall("Suma podanych liczb to: " + str(suma))
                suma = 0.0
                numbersCount = 0
                break
            pass

    finally:
        connection.close()
        pass