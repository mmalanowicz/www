from django.db import models
from django import forms
from cms.models.pluginmodel import CMSPlugin
from django.utils import timezone


class Comment(models.Model):
    username = models.CharField(max_length=100)
    content = models.CharField(max_length=200)
    created = models.DateTimeField('dodano', default=timezone.now())


class CommentPlugin(CMSPlugin):
    comments = models.ManyToManyField(Comment, blank=True, null=True)


class CommentForm(forms.Form):
    username = forms.CharField()
    content = forms.CharField(widget=forms.Textarea)