from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import Comment, CommentPlugin
from models import CommentForm
from django.utils.translation import ugettext as _


class CommentsPlugin(CMSPluginBase):
    model = CommentPlugin
    name = _("Komentarz")
    render_template = "komentarz.html"

    def render(self, context, instance, placeholder):
        request = context['request']
        if request.method == "POST":
            form = CommentForm(request.POST)
            username = request.POST['username']
            content = request.POST['content']
            if form.is_valid():
                comment = Comment(username=username, content=content)
                comment.full_clean()
                comment.save()
                instance.comments.add(comment)

        comments = instance.comments.all().order_by('-created')
        form = CommentForm()
        context.update({
            'contact': instance,
            'form': form,
            'comments': comments,
        })
        return context

plugin_pool.register_plugin(CommentsPlugin)