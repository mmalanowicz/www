from django.conf.urls import patterns, url
from django.views.generic import ListView
from django.contrib.auth.models import User
import views
from models import Entry

urlpatterns = patterns('',
                       url(r'^$', ListView.as_view(
                           template_name='microblog/stub.html',
                           queryset=Entry.objects.all().order_by('-published'),
                           context_object_name='entries',
                           ), name='list_entries'),
                       url(r'^register$', views.register_view, name='register'),
                       url(r'^login', views.login_view, name='login'),
                       url(r'^logout', views.logout_view, name='logout'),
                       url(r'^add$', views.add_entry_view, name='add_entry'),
                       url(r'^edit/(?P<id>.*)$', views.edit_entry_view, name='edit_entry'),
                       url(r'^addtag$', views.add_tag_view, name='add_tag'),
                       url(r'^userslist$', ListView.as_view(
                           template_name='microblog/userslist.html',
                           queryset=User.objects.all(),
                           context_object_name='Users',
                           ), name='users_list'),
                       url(r'^entries/(?P<username>.+)/$', views.entries_username_view, name="entries_username"),
                       url(r'^entries_m/(?P<month>.+)/$', views.entries_username_month_view, name="entries_month"),
                       url(r'^monthlist$', ListView.as_view(
                           template_name='microblog/monthslist.html',
                           queryset=Entry.objects.raw("SELECT id, strftime('%%m', published) as month, published FROM microblog_entry GROUP BY strftime('%%m', published)"),
                           context_object_name='Months',
                           ), name='months_list'),

                       #cssy, jsy
                       url(r'^src/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': '/microblog/static'}, name='staticfiles'))