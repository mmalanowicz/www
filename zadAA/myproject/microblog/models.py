from django.db import models
from django.utils import timezone
# Create your models here.


class Tag(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class Entry(models.Model):
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=500)
    published = models.DateTimeField('dodano', default=timezone.now())
    nick = models.CharField(max_length=200)
    tags = models.ManyToManyField(Tag)
    last_edited = models.DateTimeField('dodano_ostatnio', default=timezone.now())
    edited_by = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return self.title

    def published_today(self):
        now = timezone.now()
        time_delta = now - self.pub_date
        return time_delta.days == 0


class Tagzzz(models.Model):
    tag = models.ForeignKey(Tag)
    entry = models.ForeignKey(Entry)