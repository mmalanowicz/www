# -*- coding: utf-8 -*-
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.contrib import messages #system flash message'y
from django.contrib.auth import authenticate, login

from forms import AddForm, LoginForm, RegisterForm, TagForm #formularze
from models import Tag


#obsĹuga dodawania wpisu
def add_entry_view(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(redirect_to=reverse('list_entries'))

    #jestem zalogowany
    username = request.user.username
    form = AddForm(initial={'author' : username})
    if request.method == "POST":

        form = AddForm(request.POST)
        if form.is_valid():
            from models import Entry
            from django.utils import timezone
            checkboxes = request.POST.getlist('checkboxes')
            messages.success(request, checkboxes)

            listaCheckboxow = []
            for tag in checkboxes:
                listaCheckboxow.append(Tag.objects.get(name=tag))

            title = form.cleaned_data['title']
            content = form.cleaned_data['content']
            author = form.cleaned_data['author']
            entry = Entry(title=title, content=content, nick=author)
            entry.full_clean()
            entry.save()
            for tagg in listaCheckboxow:
                entry.tags.add(tagg)
            messages.success(request, 'Message added successfully')
            url = reverse('list_entries')
            return HttpResponseRedirect(redirect_to=url)
        else:
            messages.error(request, 'Invalid form submission')
            return render(request, 'microblog/add.html', {'form' : form})
    else:
        return render(request, 'microblog/add.html', {'form' : form, 'tags': Tag.objects.all()})

#obsĹuga rejestracji - nie przeczytaĹem, bo wszystko udostÄpnia juĹź Django, eh.
def register_view(request):
    form = RegisterForm()
    url = reverse('list_entries')
    if request.user.is_authenticated():
        return HttpResponseRedirect(redirect_to=url)

    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            login = form.cleaned_data['username']
            password = form.cleaned_data['passs']
            email = form.cleaned_data['email']
            user = authenticate(username=login, password=password)
            if user is None:
                #tworzymy nowego usera jeĹli juĹź taki nie istnieje
                from django.contrib.auth.models import User
                user = User.objects.create_user(login, email, password)
                user.save()
                messages.success(request, 'User registered successfully, now you can log in')
                return HttpResponseRedirect(redirect_to=url)
            else:
                messages.error(request, 'User already exists!')
        else:
            messages.error(request, 'Invalid form submission')
    return render(request, 'microblog/register.html', {'form' : form})

#obsĹuga logowania
def login_view(request):
    url = reverse('list_entries')
    if not request.user.is_authenticated():
        form = LoginForm()
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                login_ = form.cleaned_data['login']
                password_ = form.cleaned_data['password']
                #logujemy usera
                user = authenticate(username=login_, password=password_)
                if user is not None:
                # the password verified for the user
                    if user.is_active:
                        login(request, user)
                        messages.success(request, "Logged In!")
                        return HttpResponseRedirect(redirect_to=url)
                    else:
                        messages.error(request, "You were banned! Haha!")
                        return HttpResponseRedirect(redirect_to=url)
            messages.error(request, 'Invalid username or password')
        return render(request, 'microblog/login.html', {'form' : form})
    else:
        messages.success(request, 'You are already authenticated!')
        return HttpResponseRedirect(redirect_to=url)

def logout_view(request):
    from django.contrib.auth import logout
    logout(request)
    messages.success(request, 'Logged out successfully')
    return HttpResponseRedirect(redirect_to=reverse('list_entries'))

def userlist_view(request):
    from django.contrib.auth.models import User
    users = User.objects.all()
    return render(request, 'microblog/userslist.html', {'users' : users})

def entries_username_view(request, username):
    from models import Entry
    entries = Entry.objects.filter(nick=username).order_by('-published')
    return render(request, 'microblog/stub.html', {'entries' : entries})

def entries_username_month_view(request, month):
    from models import Entry
    entries = Entry.objects.filter(published__month=month).order_by('-published')
    return render(request, 'microblog/stub.html', {'entries' : entries})

#obsluga error page'y
def error404_view(request):
    return render(request, 'microblog/error.html')

def error500_view(request):
    return render(request, 'microblog/error.html')

def add_tag_view(request):
    if not request.user.is_authenticated() and request.user.username != 'admin':
        return HttpResponseRedirect(redirect_to=reverse('list_entries'))

    #jestem zalogowany
    username = request.user.username
    form = TagForm()
    if request.method == "POST":
        form = TagForm(request.POST)
        if form.is_valid():
            from models import Tag
            name = form.cleaned_data['name']
            tag = Tag(name=name)
            tag.full_clean()
            tag.save()
            messages.success(request, 'Tag added successfully')
            url = reverse('add_tag')
            return HttpResponseRedirect(redirect_to=url)
        else:
            messages.error(request, 'Invalid form submission')
            return render(request, 'microblog/tagadd.html', {'form' : form})
    else:
        return render(request, 'microblog/tagadd.html', {'form' : form})

def edit_entry_view(request, id):
    from models import Entry, Tag
    from forms import AddForm
    from django.utils import timezone
    entry = Entry.objects.get(pk = id)
    now = timezone.now()
    before = entry.published
    url = reverse('list_entries')
    isModerator = request.user.has_perm('microblog.change_entry')

    username = request.user.username
    if username != entry.nick and not isModerator:
        messages.error(request, 'Nie jesteĹ sobÄ')
        return HttpResponseRedirect(redirect_to=url)

    if (now - before).seconds > 10*60 and not isModerator:
        messages.error(request, 'Nie moĹźesz bo 10minut')
        return HttpResponseRedirect(redirect_to=url)

    if request.method == "POST":
        title = request.POST['title']
        content = request.POST['content']
        entry.edited_by = username
        entry.last_edited =timezone.now()
        entry.title = title
        entry.content = content
        entry.save()
        messages.success(request, 'Edytowano pomyĹlnie')

    messages.success(request, 'siemasz psie')
    form = AddForm(initial={'title' : entry.title, 'content' : entry.content})
    return render(request, 'microblog/edit.html', {'form' : form, 'pk' : id, 'tags': Tag.objects.all()})