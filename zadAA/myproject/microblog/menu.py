# -*- coding: utf-8 -*-

from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
import datetime
from models import Entry

class MicroblogMenu(CMSAttachMenu):
    name = _("Menu")

    def get_nodes(self, request):
        nodes = []
        userslist = NavigationNode(
            u'Lista użytkowników', reverse('users_list'), 1)
        nodes.append(userslist)

        monthslist = NavigationNode(
            u'Miesiące', reverse('months_list'), 2)
        nodes.append(monthslist)

        if not request.user.is_authenticated():
            login = NavigationNode(u'Zaloguj się', reverse('login'), 3)
            nodes.append(login)

        if request.user.is_authenticated():
            login = NavigationNode(u'Wyloguj się', reverse('logout'), 3)
            nodes.append(login)
            add = NavigationNode(u'Dodaj wpis', reverse('add_entry'), 4)
            nodes.append(add)


        users = User.objects.all()
        for user in users:
            nodes.append(
                NavigationNode(
                    user.username, reverse('entries_username', args=(user.username,)), user.pk+100, 1)
            )

        months = Entry.objects.raw("SELECT id, strftime('%%m', published) as month, published FROM microblog_entry GROUP BY strftime('%%m', published)")
        for month in months:
            nodes.append(
                NavigationNode(datetime.datetime.strftime(month.published, '%B'),
                               reverse('entries_month', args=(month.month,)),
                               month.id + 10000,
                               2)
            )
        return nodes

menu_pool.register_menu(MicroblogMenu)