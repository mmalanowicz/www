from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from microblog.menu import MicroblogMenu


class MicroblogAppHook(CMSApp):
    name = _("Microblog AppHook")
    urls = ["microblog.urls"]
    menu = [MicroblogMenu]

apphook_pool.register(MicroblogAppHook)