from django.db import models
from cms.models import CMSPlugin


class LikePlugin(CMSPlugin):
    url = models.CharField(max_length=500)
    width = models.IntegerField(default=450)