from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _
from models import LikePlugin


class CMSLikePlugin(CMSPluginBase):
    model = LikePlugin
    name = _("Like")
    render_template = "like_plugin.html"

    def render(self, context, instance, placeholder):
        context['instance'] = instance
        return context

plugin_pool.register_plugin(CMSLikePlugin)