from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class FormsAppHook(CMSApp):
    name = _("Forms AppHook")
    urls = ["forms.forms_urls"]

apphook_pool.register(FormsAppHook)