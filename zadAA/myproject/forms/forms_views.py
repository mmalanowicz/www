from django.shortcuts import render
from django.http import HttpResponseRedirect
from forms.models import ContactForm
from django.core.mail import send_mail
from django.template import RequestContext
from django.shortcuts import render_to_response


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            recipients = ['7malan@gmail.com']

            send_mail(subject, message, sender, recipients, fail_silently=True)

            return HttpResponseRedirect('/kontakt')
    else:
        form = ContactForm()

    return render_to_response('contact.html', {
        'form': form,
    }, context_instance=RequestContext(request))